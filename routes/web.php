<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Company;
use App\Product;
Route::group(['prefix'=>'/'],function (){
    Route::get('/', 'IndexController@getIndex')->name('index');
    Route::get('/register','AuthController@getRegister');
    Route::group(['prefix'=>'companies'],function (){
        Route::get('/','CompanyController@getIndex')->name('companies');
        Route::get('/{id}','CompanyController@getCompany');
    });
    Route::get('/test',function (){
        return view('register');
    });
});

