@extends('layout.main')

@section('title') لیست شرکت ها
@section('content')
    @include('layout.partials.slider')

    <div class="breadcrumbs">
        <div class="wrapper">
            <a href="{{route('index')}}">صفحه نخست</a>
            <i class="icon-arrow-left"></i>
            <a href="{{route('companies')}}">شرکت ها</a>
        </div>
    </div>

    <div class="pagetitle">
        <div class="wrapper">
            <span>لیست شرکت ها</span>
        </div>
        <b></b>
    </div>

    <div class="wrapper">
        <div class="page company">

            <div class="sort">
                <a href="?sort=newest" class="{{ (request('sort')=='newest')||!request()->has('sort')?'active':'' }}">جدیدترین</a>
                <a href="?sort=alphabet" class="{{ (request('sort')=='alphabet')?'active':'' }}">به ترتیب الفبا</a>
            </div>

            <div class="list">
                @foreach($companies as $company)
                <div class="item" data-id="{{ $company->id }}">
                    <div class="off">
                        <img src="assets/images/itemlogo.jpg"/>
                        <h2>{{ $company->title }}</h2>
                        <p>تولیدکننده مبدل های حرارتی و تجهیزات مرتبط</p>
                    </div>
                    <div class="on">
                        <strong>دسته محصولات</strong>
                        @foreach($company->categories()->unique('title')->take(4) as $cat)
                            <span>{{$cat->title}}</span>
                        @endforeach
                        <strong>برندها</strong>
                        @foreach($company->brands->take(4) as $brand)
                            <span>{{ $brand->title }}</span>
                        @endforeach
                    </div>
                </div>
                @endforeach

            </div>

        </div>
    </div>

    <div class="pagenumber">
        <div class="wrapper">
            <ul>

                <li><a href=""><i class="icon-arrow-right"></i></a></li>
                @for($i = 1 ; $i <= $companies->lastPage() ; $i++)
                    <li><a href="?page={{ $i }}{{( request()->has('sort'))?"&sort=". request('sort'):""}}">{{ $i }}</a></li>
                @endfor
                <li><a href=""><i class="icon-arrow-left"></i></a></li>

            </ul>
        </div>
    </div>

    <div class="pagetitle">
        <div class="wrapper">
            <span>اسپانسرها</span>
            <a class="more" href="">همه شرکت ها <i class="icon-arrow-left"></i></a>
        </div>
        <b></b>
    </div>

    @include('layout.partials.partner')
@endsection
