@extends('layout.main')

@php
    use App\Helpers\ImageHelper;
@endphp
@section('title')
    شرکت
    {{ $company->title }}
@endsection
@section('content')
    @include('layout.partials.slider')
<div class="breadcrumbs">
    <div class="wrapper">
        <a href="">صفحه نخست</a>
        <i class="icon-arrow-left"></i>
        <a href="">شرکت ها</a>
    </div>
</div>

<div class="wrapper">
    <div class="cotitle">

        <div class="pic">
            <img src="{{ ImageHelper::getImageUrl($company->image,'companies') }}"/>
        </div>

        <div class="text">

            <div class="right">
                <h1><a href="">شرکت {{$company->title}}<i class="fas fa-link"></i></a></h1>
                <p>{{ $company->address }} | <a href="">نقشه</a></p>
                <p>شماره تماس: {{ $company->phone }}</p>
            </div>

            <div class="left">
                <div class="row">
                    <a class="button small" href="">ذخیره شرکت</a>
                    <a class="button small" href="">اضافه به لیست کوتاه</a>
                </div>
                <div class="row">
                    <a class="button big" href="">تماس با شرکت</a>
                </div>
            </div>

        </div>

    </div>
</div>

<div class="toptabs">
    <div class="wrapper">

        <ul>
            <li class="toptab active"><a href="">اطلاعات کلی</a></li>
            <li class="toptab"><a href="">محصولات</a></li>
            <li class="toptab"><a href="">پروژه ها</a></li>
            <li class="toptab"><a href="">اخبار و مقالات</a></li>
            <li class="toptab"><a href="">وبینار ها</a></li>
            <li class="toptab"><a href="">نمایشگاه ها</a></li>
            <li class="toptab"><a href="">اطلاعات تماس</a></li>
        </ul>

        <div class="toptexts">
            <div class="toptext">

                <div class="text right">
                    <h2>محصولات و برندها</h2>
                    <ul class="brandtabs">
                        <li class="brandtab brandtab1 active">محصولات</li>
                        <li class="brandtab brandtab2">برندها</li>
                    </ul>
                    <div class="brandtexts">
                        <div class="brandtext brandtext1 active">
                            <!-- TODO: Add link to each category -->
                            @foreach($categories as $pc)
                                <p><a href=""><i class="icon-arrow-left"></i>{{ $pc->title }}</a></p>
                            @endforeach
                        </div>
                        <div class="brandtext brandtext2">
                            @foreach($company->brands as $brand)
                            <p><a href=""><i class="icon-arrow-left"></i>{{ $brand->title }}</a></p>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="text left">
                    <h2>جزئیات شرکت</h2>
                    <div class="coinfo">
                        <p><strong>وبسایت</strong><span><a href="">{{ $company->website }}<i class="fas fa-link"></i></a></span></p>
                        <p><strong>گراهینامه ها</strong>???<span></span></p>
                        <p><strong>نوع فعالیت شرکت</strong><span>{{ $company->activity_type() }}</span></p>
                        <p><strong>افراد کلیدی</strong><span>???</span></p>
                        <p><strong>فعالیت اصلی شرکت</strong>{{ $company->main_activity }}<span></span></p>
                        <p><strong>سال تاسیس</strong><span>{{ $company->established_year }}</span></p>
                        <p><strong>شبکه های اجتماعی</strong><a href="{{$company->fb_url}}"><i class="fab fa-facebook"></i></a><a href="{{ $company->linkedin_url }}"><i class="fab fa-linkedin"></i></a><a href="{{$company->insta_url}}"><i class="fab fa-instagram"></i></a><span></span></p>
                        <p><strong>تعداد کارکنان</strong><span>???</span></p>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>

<div class="wrapper">
    <div class="textabs">
        <ul>
            <li class="textab textab1 active">درباره شرکت</li>
            <li class="textab textab2">پروژه ها</li>
            <li class="textab textab3">اخبار و مقالات</li>
            <li class="textab textab4">وبینار ها</li>
            <li class="textab textab5">نمایشگاه ها</li>
        </ul>
        <div class="texts">
            <div class="textp textp1 active">
                <h2>توصیف شرکت</h2>
                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد وزمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد. </p>
                <h3>ماموریت ها:</h3>
                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. </p>
                <h3>چشم اندازها:</h3>
                <p>کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت.</p>
            </div>
            <div class="textp textp2">
                <h2>پروژه ها</h2>
                <p>کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت.</p>
            </div>
            <div class="textp textp3">
                <h2>اخبار و مقالات</h2>
                <p>کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت.</p>
            </div>
            <div class="textp textp4">
                <h2>وبینار ها</h2>
                <p>کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت.</p>
            </div>
            <div class="textp textp5">
                <h2>نمایشگاه ها</h2>
                <p>کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت.</p>
            </div>
        </div>
    </div>
</div>

<div class="wrapper">
    <div class="coitems">
        <h2>محصولات</h2>
        <ul>
            @for($i = 0 ; $i < count($categories) ; $i++ )
                <li class={{ ( $i == 0 ) ? "active":"" }}>{{ $categories[$i]->title }}</li>
            @endfor
        </ul>
        <div class="itemtexts">
            @for($i = 0 ; $i < count($categories) ; $i++ )
            <div class="itemtext itemtext{{$i+1}} {{ ($i == 0)? "active":"" }}">

                <div class="products">
                    <div class="list">
                    @foreach($products as $product)
                        @if($product->title == $categories[$i]->title)
                                <div class="item">
                                    <div class="pic">
                                        <div class="off">
                                            <a href=""><img src="/assets/images/pic.jpg"/></a>
                                        </div>
                                        <div class="on">
                                            <img src="/assets/images/itemlogo.jpg"/>
                                            <h2>طاها قالب توس</h2>
                                        </div>
                                    </div>
                                    <div class="text">
                                        <a href="">{{ $product->title }}</a>
                                        <span>فشار بالا / حداکثر دما 15C</span>
                                        <img src="/assets/images/itembrand.jpg"/>
                                    </div>
                                </div>
                        @endif
                    @endforeach
                    </div>
                </div>

            </div>
            @endfor

        </div>
    </div>
</div>

<div class="pagetitle">
    <div class="wrapper">
        <span>شرکت های مشابه</span>
        <a class="more" href="">همه شرکت ها <i class="icon-arrow-left"></i></a>
    </div>
    <b></b>
</div>

<div class="wrapper">
    <div class="page company">

        <div class="list">

            <div class="item">
                <div class="off">
                    <img src="/assets/images/itemlogo.jpg"/>
                    <h2>طاها قالب توس</h2>
                    <p>تولیدکننده مبدل های حرارتی و تجهیزات مرتبط</p>
                </div>
                <div class="on">
                    <strong>دسته محصولات</strong>
                    <span>مبدل های حرارتی</span>
                    <span>جک های هیدرولیکی</span>
                    <span>مبدل های حرارتی</span>
                    <span>جک های هیدرولیکی</span>
                    <strong>برندها</strong>
                    <span>TGT</span>
                    <span>Funke</span>
                    <span>SKF</span>
                    <span>Pentax</span>
                </div>
            </div>
            <div class="item">
                <div class="off">
                    <img src="/assets/images/itemlogo.jpg"/>
                    <h2>طاها قالب توس</h2>
                    <p>تولیدکننده مبدل های حرارتی و تجهیزات مرتبط</p>
                </div>
                <div class="on">
                    <strong>دسته محصولات</strong>
                    <span>مبدل های حرارتی</span>
                    <span>جک های هیدرولیکی</span>
                    <span>مبدل های حرارتی</span>
                    <span>جک های هیدرولیکی</span>
                    <strong>برندها</strong>
                    <span>TGT</span>
                    <span>Funke</span>
                    <span>SKF</span>
                    <span>Pentax</span>
                </div>
            </div>
            <div class="item">
                <div class="off">
                    <img src="/assets/images/itemlogo.jpg"/>
                    <h2>طاها قالب توس</h2>
                    <p>تولیدکننده مبدل های حرارتی و تجهیزات مرتبط</p>
                </div>
                <div class="on">
                    <strong>دسته محصولات</strong>
                    <span>مبدل های حرارتی</span>
                    <span>جک های هیدرولیکی</span>
                    <span>مبدل های حرارتی</span>
                    <span>جک های هیدرولیکی</span>
                    <strong>برندها</strong>
                    <span>TGT</span>
                    <span>Funke</span>
                    <span>SKF</span>
                    <span>Pentax</span>
                </div>
            </div>
            <div class="item">
                <div class="off">
                    <img src="/assets/images/itemlogo.jpg"/>
                    <h2>طاها قالب توس</h2>
                    <p>تولیدکننده مبدل های حرارتی و تجهیزات مرتبط</p>
                </div>
                <div class="on">
                    <strong>دسته محصولات</strong>
                    <span>مبدل های حرارتی</span>
                    <span>جک های هیدرولیکی</span>
                    <span>مبدل های حرارتی</span>
                    <span>جک های هیدرولیکی</span>
                    <strong>برندها</strong>
                    <span>TGT</span>
                    <span>Funke</span>
                    <span>SKF</span>
                    <span>Pentax</span>
                </div>
            </div>
            <div class="item">
                <div class="off">
                    <img src="/assets/images/itemlogo.jpg"/>
                    <h2>طاها قالب توس</h2>
                    <p>تولیدکننده مبدل های حرارتی و تجهیزات مرتبط</p>
                </div>
                <div class="on">
                    <strong>دسته محصولات</strong>
                    <span>مبدل های حرارتی</span>
                    <span>جک های هیدرولیکی</span>
                    <span>مبدل های حرارتی</span>
                    <span>جک های هیدرولیکی</span>
                    <strong>برندها</strong>
                    <span>TGT</span>
                    <span>Funke</span>
                    <span>SKF</span>
                    <span>Pentax</span>
                </div>
            </div>
            <div class="item">
                <div class="off">
                    <img src="/assets/images/itemlogo.jpg"/>
                    <h2>طاها قالب توس</h2>
                    <p>تولیدکننده مبدل های حرارتی و تجهیزات مرتبط</p>
                </div>
                <div class="on">
                    <strong>دسته محصولات</strong>
                    <span>مبدل های حرارتی</span>
                    <span>جک های هیدرولیکی</span>
                    <span>مبدل های حرارتی</span>
                    <span>جک های هیدرولیکی</span>
                    <strong>برندها</strong>
                    <span>TGT</span>
                    <span>Funke</span>
                    <span>SKF</span>
                    <span>Pentax</span>
                </div>
            </div>

        </div>

    </div>
</div>

@endsection
