@extends('layout.main')
@section('title')
    صفحه ی اصلی
@endsection
@section('content')
<div class="breadcrumbs">
    <div class="wrapper">
        <a href="">صفحه نخست</a>
        <i class="icon-arrow-left"></i>
        <a href="">ثبت نام / ورود</a>
    </div>
</div>

<div class="wrapper">
    <div class="register-content">
        <h2>پروفایل رایگان IranSpec.com شرکت خود را ایجاد کنید.</h2>
        <h3>شرکت خود را در بهترین و کامل ترین پلتفرم بازاریابی صنعتی کشور قرار دهید.</h3>
        <div class="register-other">
            حساب کاربری دارید؟
            <a href="#">ورود</a>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="register-box">
                    <div class="row">
                        <div class="col-6">
                            <input class="register-input d-block w-100" type="text" placeholder="نام">
                        </div>
                        <div class="col-6">
                            <input class="register-input d-block w-100" type="text" placeholder="نام خانوادگی">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <input class="register-input d-block w-100" type="text" placeholder="سمت">
                        </div>
                        <div class="col-6">
                            <input class="register-input d-block w-100" type="text" placeholder="نام شرکت">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <input class="register-input d-block w-100" type="text" placeholder="تعداد کارمندان">
                        </div>
                        <div class="col-6">
                            <select class="register-input d-block w-100">
                                <option disabled selected>حوزه فعالیت</option>
                                <option>صنعت</option>
                                <option>تجارت</option>
                                <option>کشاورزی</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <select class="register-input d-block w-100">
                                <option disabled selected>استان</option>
                                <option>تهران</option>
                                <option>فارس</option>
                                <option>اصفهان</option>
                            </select>
                        </div>
                        <div class="col-6">
                            <select class="register-input d-block w-100">
                                <option disabled selected>شهر</option>
                                <option>فسا</option>
                                <option>شیراز</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <input class="register-input d-block w-100" type="text" placeholder="آدرس پستی">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <input class="register-input d-block w-100" type="text" placeholder="شماره تلفن">
                        </div>
                        <div class="col-6">
                            <input class="register-input d-block w-100" type="text" placeholder="شماره موبایل">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <input class="register-input d-block w-100" type="text" placeholder="آدرس ایمیل">
                        </div>
                        <div class="col-6">
                            <input class="register-input d-block w-100" type="text" placeholder="آدرس وبسایت">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <button type="submit" class="register-submit">پروفایل رایگان من را ایجاد کن</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6 register-text">
                <p><a href="#">پروفایل رایگان</a> به شما کمک میکند تا توسط مهندسان، خریداران صنعتی و بطور کلی افراد موثر در چرخه خرید صنعتی یافته شوید.</p>
                <br>
                <p>پروفایل رایگان <a href="#">امکانات پایه</a> زیر را در اختیار شما قرار میدهد:</p>
                <br>
                <ul>
                    <li>
                        <strong>دسته بندی محصولات و خدمات :</strong>
                        محصولات و خدمات خود را در دسته بندی بخصوص خود قرار دهید و ویژگی های خاص از پیش تعریف شده برای هر محصول را تعیین کنید.
                    </li>
                    <li>
                        <strong>لیست شدن در جستجوها :</strong>
                        در نتایج جستجو ایران اسپک، در محصولات ، خدمات و شرکت ها لیست میشوید و مخاطب صنعتی به اطلاعات محصولات و شرکت شما دسترسی خواهد داشت.
                    </li>
                </ul>
                <strong>برای شروع فرم را پر کنید.</strong>
            </div>
        </div>
    </div>
</div>
@endsection
