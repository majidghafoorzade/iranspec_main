@extends('layout.main')
@php
    use App\Helpers\ImageHelper;
@endphp
@section('title')
    صفحه ی اصلی
@endsection
@section('content')
    @include('layout.partials.slider')

    <div class="indexsearch">
        <div class="wrapper">
            <h1>ایران اسپک برای صنعت کشور</h1>
            <form>
                <select>
                    <option>محصولات</option>
                    <option>شرکت ها</option>
                    <option>پروژه ها</option>
                    <option>SPECmag</option>
                </select>
                <input type="text" placeholder="دسته محصول ، برند ، مدل و ..."/>
                <input type="submit" value="جستجو"/>
            </form>
            <ul>
                <li><strong>{{ $count['products'] }}</strong> محصول</li>
                <li><strong>{{ $count['companies'] }}</strong> شرکت</li>
                <li><strong>{{ $count['brands'] }}</strong> برند</li>
                <li><strong>۲۲۰۰</strong> محصول(فیلد استاتیک)</li>
            </ul>
            <a href="" class="catmore">دسته بندی محصولات <i class="icon-arrow-left"></i></a>
        </div>
    </div>

    <div class="pagetitle">
        <div class="wrapper">
            <span>محصولات اسپانسر دار</span>
        </div>
        <b></b>
    </div>

    <div class="wrapper">
        <div class="page products">

            <div class="list owl-carousel owl-theme">

                <div class="item">
                    <div class="pic">
                        <div class="off">
                            <a href=""><img src="assets/images/pic.jpg"/></a>
                        </div>
                        <div class="on">
                            <img src="assets/images/itemlogo.jpg"/>
                            <h2>طاها قالب توس</h2>
                        </div>
                    </div>
                    <div class="text">
                        <a href="">مبدل حرارتی صفحه ای گسکت دار  سری FP/FPS</a>
                        <span>فشار بالا / حداکثر دما 15C</span>
                        <img src="assets/images/itembrand.jpg"/>
                    </div>
                </div>
                <div class="item">
                    <div class="pic">
                        <div class="off">
                            <a href=""><img src="assets/images/pic.jpg"/></a>
                        </div>
                        <div class="on">
                            <img src="assets/images/itemlogo.jpg"/>
                            <h2>طاها قالب توس</h2>
                        </div>
                    </div>
                    <div class="text">
                        <a href="">مبدل حرارتی صفحه ای گسکت دار  سری FP/FPS</a>
                        <span>فشار بالا / حداکثر دما 15C</span>
                        <img src="assets/images/itembrand.jpg"/>
                    </div>
                </div>
                <div class="item">
                    <div class="pic">
                        <div class="off">
                            <a href=""><img src="assets/images/pic.jpg"/></a>
                        </div>
                        <div class="on">
                            <img src="assets/images/itemlogo.jpg"/>
                            <h2>طاها قالب توس</h2>
                        </div>
                    </div>
                    <div class="text">
                        <a href="">مبدل حرارتی صفحه ای گسکت دار  سری FP/FPS</a>
                        <span>فشار بالا / حداکثر دما 15C</span>
                        <img src="assets/images/itembrand.jpg"/>
                    </div>
                </div>
                <div class="item">
                    <div class="pic">
                        <div class="off">
                            <a href=""><img src="assets/images/pic.jpg"/></a>
                        </div>
                        <div class="on">
                            <img src="assets/images/itemlogo.jpg"/>
                            <h2>طاها قالب توس</h2>
                        </div>
                    </div>
                    <div class="text">
                        <a href="">مبدل حرارتی صفحه ای گسکت دار  سری FP/FPS</a>
                        <span>فشار بالا / حداکثر دما 15C</span>
                        <img src="assets/images/itembrand.jpg"/>
                    </div>
                </div>
                <div class="item">
                    <div class="pic">
                        <div class="off">
                            <a href=""><img src="assets/images/pic.jpg"/></a>
                            <span class="new">NEW</span>
                        </div>
                        <div class="on">
                            <img src="assets/images/itemlogo.jpg"/>
                            <h2>طاها قالب توس</h2>
                        </div>
                    </div>
                    <div class="text">
                        <a href="">مبدل حرارتی صفحه ای گسکت دار  سری FP/FPS</a>
                        <span>فشار بالا / حداکثر دما 15C</span>
                        <img src="assets/images/itembrand.jpg"/>
                    </div>
                </div>
                <div class="item">
                    <div class="pic">
                        <div class="off">
                            <a href=""><img src="assets/images/pic.jpg"/></a>
                            <span class="new">NEW</span>
                        </div>
                        <div class="on">
                            <img src="assets/images/itemlogo.jpg"/>
                            <h2>طاها قالب توس</h2>
                        </div>
                    </div>
                    <div class="text">
                        <a href="">مبدل حرارتی صفحه ای گسکت دار  سری FP/FPS</a>
                        <span>فشار بالا / حداکثر دما 15C</span>
                        <img src="assets/images/itembrand.jpg"/>
                    </div>
                </div>
            </div>


        </div>

        <div class="pagetitle bottomtitle"><a class="more" href="">همه محصولات اسپانسر دار <i class="icon-arrow-left"></i></a></div>

    </div>

    <div class="pagetitle">
        <div class="wrapper">
            <span>محصولات جدید</span>
        </div>
        <b></b>
    </div>

    <div class="wrapper">
        <div class="page products">

            <div class="list new">
                @foreach($products as $p)
                <div class="item">
                    <div class="pic">
                        <div class="off">
                            <a href=""><img src="{{ ImageHelper::getImageUrl($p->image(),'products') }}"/></a>
                            <span class="new">NEW</span>
                        </div>
                        <div class="on">
                            <img src="{{ ($p->company() != null )? ImageHelper::getImageUrl($p->company()->image,'companies'):'no_image.jpg' }}"/>
                            <h2>{{ ($p->company())?$p->company()->title:'' }}</h2>
                        </div>
                    </div>
                    <div class="text">
                        <a href="">{{ $p->title }}</a>
                        <span>فشار بالا / حداکثر دما 15C</span>
                        <img src="{{ ImageHelper::getImageUrl($p->brand->image,'brands') }}"/>
                    </div>
                </div>
                @endforeach

            </div>

        </div>

        <div class="pagetitle bottomtitle"><a class="more" href="">همه محصولات جدید <i class="icon-arrow-left"></i></a></div>

    </div>

    <div class="projects graybox">
        <div class="wrapper">

            <div class="pagetitle">
                <span>پروژه های جدید</span>
                <a class="more" href="">پروژه های بیشتر <i class="icon-arrow-left"></i></a>
                <b></b>
            </div>

            <div class="posts">

                <div class="post">
                    <a href=""><img src="assets/images/post.jpg"/></a>
                    <h3><a href="">طاها قالب طوس</a></h3>
                    <p>بزرگترین مبدل های حرارتی ایران با ظرفیت 5 مگاوات در پتروشیمی پردیس</p>
                </div>
                <div class="post">
                    <a href=""><img src="assets/images/post.jpg"/></a>
                    <h3><a href="">طاها قالب طوس</a></h3>
                    <p>بزرگترین مبدل های حرارتی ایران با ظرفیت 5 مگاوات در پتروشیمی پردیس</p>
                </div>
                <div class="post">
                    <a href=""><img src="assets/images/post.jpg"/></a>
                    <h3><a href="">طاها قالب طوس</a></h3>
                    <p>بزرگترین مبدل های حرارتی ایران با ظرفیت 5 مگاوات در پتروشیمی پردیس</p>
                </div>

            </div>

        </div>
    </div>

    <div class="pagetitle">
        <div class="wrapper">
            <span>اسپانسرها</span>
            <a class="more" href="">همه شرکت ها <i class="icon-arrow-left"></i></a>
        </div>
        <b></b>
    </div>

    @include('layout.partials.partner')

    <div class="blog graybox">
        <div class="wrapper">

            <div class="posts">

                <div class="blogtitle">
                    <span><b>SPEC</b>mag</span> مجله ایران اسپک
                </div>

                <div class="big">
                    <div class="post" data-id="{{ $blogPosts[0]['id'] }}">
                        <a href="{{ $blogPosts[0]['link'] }}"><img src="{{ $blogPosts[0]['image'] }}"/></a>
                        <p><a href="">بازاریابی صنعتی</a></p>
                        <h3><a href="">طاها قالب طوس</a></h3>
                        <p>{{ $blogPosts[0]['description']}}</p>
                    </div>
                </div>
                <div class="small">
                    @for( $i = 1 ; $i < count($blogPosts) ; $i++ )
                    <div class="post">
                        <a href="{{ $blogPosts[$i]['link'] }}"><img src="{{ $blogPosts[$i]['image'] }}"/></a>
                        <p><a href="">علوم و تکنولوژی</a></p>
                        <h3><a href="">طاها قالب طوس</a></h3>
                    </div>
                    @endfor
                </div>

            </div>

        </div>
    </div>

    <div class="pagetitle">
        <div class="wrapper">
            <span>دسته بندی محصولات</span>
            <a class="more" href="">همه محصولات <i class="icon-arrow-left"></i></a>
        </div>
        <b></b>
    </div>

    <div class="category">
        <div class="wrapper">
            <ul>
                @for( $i = 0 ; $i < count($categories)/2 ; $i++ )
                <li>
                    <p><a>{{ $categories[$i]->title }}<i class="icon-arrow-down"></i><span>{{ $categories[$i]->entitle }}</span></a></p>
                    <div class="open">
                        @foreach( $categories[$i]->subcategories as $sub )
                        <a href="">{{ $sub->title }}<span>{{ $sub->entitle }}</span></a>
                        @endforeach
                    </div>
                </li>
                @endfor
            </ul>
            <ul>
                @for( ; $i < count($categories) ; $i++ )
                <li>
                    <p><a>{{ $categories[$i]->title }}<i class="icon-arrow-down"></i><span>{{ $categories[$i]->entitle }}</span></a></p>
                    <div class="open">
                        @foreach( $categories[$i]->subcategories as $sub )
                            <a href="">{{ $sub->title }}<span>{{ $sub->entitle }}</span></a>
                        @endforeach
                    </div>
                </li>
                @endfor
            </ul>
        </div>
    </div>


@endsection
