<div class="sticky">

    <div class="bar">
        <div class="wrapper">

            <form class="search">
                <input type="text" placeholder="محصولات ، شرکت ها ، پروژه ها ، مقالات و ...">
                <input type="submit" value=""/>
            </form>

            <div class="call">
                <p>Tel: <span>021-123456789</span></p>
                <p>Email: <span>info@iranspec.ir</span></p>
            </div>

        </div>
    </div>

    <div class="header">
        <div class="wrapper">

            <div class="logo">
                <a href=""><img src="/assets/images/logo.png"/></a>
            </div>

            <div class="menu">
                <div class="login">
                    <a href=""><i class="icon-arrow-down"></i> <strong>حساب کاربری</strong> <i class="fas fa-user-circle"></i></a>
                    <div class="loginbox">
                        <a href="" class="button">ورود به ایران اسپک</a>
                        <p>کاربر جدید هستید؟ <a href="">ثبت نام</a></p>
                        <p><a href=""><span><i class="fas fa-user"></i> فراموشی کلمه عبور</span></a></p>
                        <p><a href=""><span><i class="fas fa-phone"></i> تماس با پشتیبانی</span></a></p>
                    </div>
                </div>
                <ul>
                    <li class="active"><a href="">به ما بپیوندید</a></li>
                    <li>
                        <a href="">شبکه صنعت</a>
                        <ul>
                            <li><a href="">زیر منو</a></li>
                            <li><a href="">شبکه صنعت</a></li>
                            <li><a href="">آزمایشی</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="">بازاریابی صنعتی</a>
                        <ul>
                            <li><a href="">زیر منو</a></li>
                            <li><a href="">شبکه صنعت</a></li>
                            <li><a href="">آزمایشی</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="">SPECmag</a>
                        <ul>
                            <li><a href="">زیر منو</a></li>
                            <li><a href="">شبکه صنعت</a></li>
                            <li><a href="">آزمایشی</a></li>
                        </ul>
                    </li>
                    <li><a href="">درباره ما</a></li>
                </ul>
            </div>

        </div>
    </div>

</div>
<div class="stickyend"></div>
