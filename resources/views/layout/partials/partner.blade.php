@php
use App\Helpers\ImageHelper;
@endphp
<div class="partner">
    <div class="wrapper">
        <ul class="owl-carousel owl-theme">
            @foreach( $partners as $partner)
                <li class="item"><a href="{{ $partner->url }}"><img src="{{ ImageHelper::getImageUrl($partner->image,'partners') }}"/></a></li>
            @endforeach
        </ul>
    </div>
</div>
