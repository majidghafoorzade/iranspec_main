<div class="about">
    <div class="wrapper">
        <img src="/assets/images/logo.png"/>
        <p><strong>توسط مهندسین و خریداران صنعتی دیده شوید</strong> لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است</p>
        <a href="">به ما بپیوندید</a>
    </div>
</div>

<div class="footer">
    <div class="wrapper">

        <div class="box">
            <strong>شبکه صنعت</strong>
            <ul>
                <li><a href="">دسته بندی محصولات</a></li>
                <li><a href="">پربازدیدترین ها</a></li>
                <li><a href="">پربازدیدترین دسته</a></li>
                <li><a href="">لیست شرکت ها</a></li>
                <li><a href="">پروژه ها</a></li>
            </ul>
        </div>
        <div class="box">
            <strong>مجله SPECmag</strong>
            <ul>
                <li><a href="">دسته بندی محصولات</a></li>
                <li><a href="">پربازدیدترین ها</a></li>
                <li><a href="">پربازدیدترین دسته</a></li>
                <li><a href="">لیست شرکت ها</a></li>
                <li><a href="">پروژه ها</a></li>
            </ul>
        </div>
        <div class="box">
            <strong>بازاریابی صنعتی</strong>
            <ul>
                <li><a href="">دسته بندی محصولات</a></li>
                <li><a href="">پربازدیدترین ها</a></li>
                <li><a href="">پربازدیدترین دسته</a></li>
                <li><a href="">لیست شرکت ها</a></li>
                <li><a href="">پروژه ها</a></li>
            </ul>
        </div>
        <div class="box">
            <strong>درباره ما</strong>
            <ul>
                <li><a href="">دسته بندی محصولات</a></li>
                <li><a href="">پربازدیدترین ها</a></li>
                <li><a href="">پربازدیدترین دسته</a></li>
                <li><a href="">لیست شرکت ها</a></li>
                <li><a href="">پروژه ها</a></li>
            </ul>
        </div>

    </div>
</div>

<div class="copyright">
    <div class="wrapper">

        <div class="farsi">
            <strong>قوانین و مقررات</strong>
            <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.</p>
        </div>

        <div class="en">
            <strong>IRANSPEC</strong>
            <p>Copyrights <i class="fas fa-copyright"></i> All Rights Reserved 2018. Designed by <a href="https://redgraphic.ir/" target="_blank">RedGraphic</a></p>
        </div>

    </div>
</div>

<div class="mobilemenu">
    <div class="wrapper">
        <a href=""><span class="mobilelogo"><img src="/assets/images/logo.png"/></span></a>
        <i id="mobiledo" class="icon-menu mobiledo"></i>
        <div class="mobilewrap">
            <ul class="mobilenav">
                <li><a href="">به ما بپیوندید</a></li>
                <li><a href="">شبکه صنعت</a></li>
                <li><a href="">بازاریابی صنعتی</a></li>
                <li><a href="">SPECmag</a></li>
                <li><a href="">درباره ما</a></li>
            </ul>
            <i id="doclose" class="icon-arrow-up doclose"></i>
        </div>
    </div>
</div>

<script src="/assets/jquery.min.js"></script>
<script src="/assets/main.js"></script>
<script src="/assets/owl.carousel.js"></script>
