<!-- Designed and Developed by www.RedGraphic.ir -->
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="/style.css">
    @yield('head-styles')
    @yield('head-scripts')
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="HandheldFriendly" content="true">
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1,user-scalable=no">
</head>
<body>

@include('layout.header')

@yield('content')

@include('layout.footer')
@yield('footer-scripts')
</body>
</html>
