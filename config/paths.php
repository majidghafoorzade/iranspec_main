<?php

return [
    'images' => [
        'asset' => 'images/assets/',
        'brands' => '/assets/images/brand',
        'partners' => '/assets/images/partner',
        'products' => '/assets/images/product',
        'projects' => '/assets/images/project',
        'companies'=> '/assets/images/company',
        'partners'=> '/assets/images/partner',
    ]
];
