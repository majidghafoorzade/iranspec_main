<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Category;
use App\Company;
use App\Partner;
use App\Product;
use App\Helpers\ApiHelper;

class IndexController extends Controller
{
    public function getIndex(){
        $count = $this->getSummeryCount();
        $blogPosts = ApiHelper::getBlogPosts();
        $products = Product::take(15)->get();
        $partners = Partner::take(20)->get();
        $categories = Category::all();
        return view('index',compact(
            'count','products',
            'partners','categories','blogPosts'
        ));
    }

    private function getSummeryCount(){
        $count['products'] = Product::count();
        $count['companies'] = Company::count();
        $count['brands'] = Brand::count();
        return $count;
    }
}
