<?php

namespace App\Http\Controllers;

use App\Company;
use App\Partner;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function getIndex(){
        $sortByColumns = array(
            'alphabet' => 'title',
            'newest' => 'id'
        );
        if( request()->has('sort') ){
            $sortBy = request('sort');
            $sortBy = $sortByColumns[$sortBy];
        }else{
            $sortBy='id';
        }
        $companies = Company::orderBy($sortBy,'desc')->paginate(30);
        $partners = Partner::take(20)->get();
        return view('company.index',compact(
            'companies','sortBy','partners'
        ));
    }

    public function getCompany($id){
        $company = Company::findOrFail($id);
        $result = $this->getProductsAndCategories($id);
        $categories = $company->categories()->unique('title');
        $products = $result['products'];
        return view('company.post',compact('company','categories','products'));
    }

    private function getProductsAndCategories($company_id){
        $productCategories = DB::table('companies')->where('companies.id','=',$company_id)
            ->join('companies_products','companies.id','=','companies_products.company_id')
            ->join('products','companies_products.product_id','=','products.id')
            ->join('productcategories_products','companies_products.product_id','=','productcategories_products.product_id')
            ->join('productcategories','productcategories.id','=','productcategories_products.productcategory_id');
        $ret_val['categories'] = $productCategories->select('productcategories.title')->distinct()->get();
        $ret_val['products'] = $productCategories->select('products.*','productcategories.title')->get();
        return $ret_val;
    }

    private function getCategories($company_id){
        $result = $this->getProductsAndCategories($company_id);
        return $result['categories'];
    }
}
