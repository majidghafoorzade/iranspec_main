<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    public function brands(){
        return $this->belongsToMany('App\Brand','companies_brands','company_id','brand_id','id');
    }

    public function products(){
        return $this->belongsToMany('App\Product','companies_products','company_id','product_id','id');
    }

    public function activity_type(){
        switch ($this->activity_type){
            case "t":
                return "تولیدی";
                break;
            case "b":
                return "بازرگانی";
                break;
            case "k":
                return "خدماتی";
                break;
        }
    }
    public function categories(){
        return $this->products->load('categories')->pluck('categories')->collapse();
    }
}
