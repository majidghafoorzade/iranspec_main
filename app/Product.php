<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function brand(){
        return $this->belongsTo('App\Brand');
    }
    public function image(){
        $productImage = $this->hasOne('App\ProductImage')->orderBy('id')->take(1)->get();
        if(count($productImage) >= 1){
            return $productImage[0]->image_address;
        }else{
            return 'no_image.jpg';
        }
    }
    public function company(){
        $companies = $this->belongsToMany('App\Company','companies_products','product_id','company_id','id')->get();
        if(count($companies) >= 1){
            return $companies[0];
        }else{
            return null;
        }
    }

    public function categories(){
        return $this->belongsToMany('App\Productcategory','productcategories_products');
    }
}
