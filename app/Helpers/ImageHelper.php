<?php


namespace App\Helpers;


class ImageHelper
{
    public static function getImageUrl($image_name,$image_type){
        $base_path = config("paths.images.$image_type");
        $image_path = $base_path.'/'.$image_name;
        return $image_path;
    }
}
