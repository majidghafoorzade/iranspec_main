<?php


namespace App\Helpers;


class ApiHelper
{
    private static $blog_posts_url = "http://mag.iranspec.com/wp-json/wp/v2/posts?per_page=5";
    private static $blog_post_image_url = "http://mag.iranspec.com/wp-json/wp/v2/media/";

    public static function getBlogPosts(){
        $request_result = file_get_contents(ApiHelper::$blog_posts_url);
        $json_posts = json_decode($request_result);
        for ( $i = 0 ; $i < count($json_posts) ; $i++ ){
            $json_post = $json_posts[$i];
            $post['id'] = $json_post->id;
            $post['title'] = $json_post->title->rendered;
            $post['link'] = $json_post->link;
            $post['description'] = strip_tags($json_post->excerpt->rendered);
            $post['image'] = ApiHelper::getBlogImage($json_post->featured_media);
            $posts[$i] = $post;
        }
        return $posts;
    }

    public static function getBlogImage($id){
        $media_url = ApiHelper::$blog_post_image_url . $id;
        $result = json_decode(file_get_contents($media_url));
        $image = $result->source_url;
        return $image;
    }
}
