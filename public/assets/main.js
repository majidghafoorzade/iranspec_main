$(document).ready(function(){  $('#mobiledo').click(function(){  $('.mobilewrap').addClass('active');  });  });
$(document).ready(function(){  $('#mobiledo').click(function(){  $('.mobiledo').removeClass('icon-menu');  });  });

$(document).ready(function(){  $('#doclose').click(function(){  $('.mobilewrap').removeClass('active');  });  });
$(document).ready(function(){  $('#doclose').click(function(){  $('.mobiledo').addClass('icon-menu');  });  });

$(document).ready(function(){
	
	$('.company .list .item').click(function(){
		$('.company .list .item').removeClass('active');  
		$(this).addClass('active');
	});

});
	
$(document).ready(function(){

	$('.category ul li').click(function(){
	$(this).find( '.open' ).slideToggle();
	  });
	  
});

	$(document).ready(function(){

	$('.sidebar .widget h3').click(function(){
	$(this).next( '.text' ).toggle();
	  });
	 
	});	

	
$(document).ready(function(){

	$('.brandtabs li').click(function(){
	$('.brandtabs li').removeClass( 'active' );
	$(this).addClass( 'active' );
	  });
	  
	$('.brandtab1').click(function(){
	$('.brandtext2').removeClass( 'active' );
	$('.brandtext1').addClass( 'active' );
	  });	 
	  
	$('.brandtab2').click(function(){
	$('.brandtext1').removeClass( 'active' );
	$('.brandtext2').addClass( 'active' );
	  });
	  
});
	
$(document).ready(function(){

	$('.textabs li').click(function(){
	$('.textabs li').removeClass( 'active' );
	$(this).addClass( 'active' );
	  });
	  
	$('.textab1').click(function(){
	$('.textp').removeClass( 'active' );
	$('.textp1').addClass( 'active' );
	  });	 
	$('.textab2').click(function(){
	$('.textp').removeClass( 'active' );
	$('.textp2').addClass( 'active' );
	  });	 
	$('.textab3').click(function(){
	$('.textp').removeClass( 'active' );
	$('.textp3').addClass( 'active' );
	  });	 
	$('.textab4').click(function(){
	$('.textp').removeClass( 'active' );
	$('.textp4').addClass( 'active' );
	  });	 
	$('.textab5').click(function(){
	$('.textp').removeClass( 'active' );
	$('.textp5').addClass( 'active' );
	  });	 
	  
});	

$(document).ready(function(){

	$('.coitems li').click(function(){
	$('.coitems li').removeClass( 'active' );
	$(this).addClass( 'active' );
	  });
	  
	$('.coitems li:nth-child(1)').click(function(){
	$('.itemtext').removeClass( 'active' );
	$('.itemtext1').addClass( 'active' );
	  });	
	$('.coitems li:nth-child(2)').click(function(){
	$('.itemtext').removeClass( 'active' );
	$('.itemtext2').addClass( 'active' );
	  });	
	$('.coitems li:nth-child(3)').click(function(){
	$('.itemtext').removeClass( 'active' );
	$('.itemtext3').addClass( 'active' );
	  });	
	  
});


$(document).ready(function() {
  var owl = $('.slider .owl-carousel');
  owl.owlCarousel({
    rtl: true,
    margin: 0,
    nav: true,
    navText : ["<i class='icon-arrow-right'></i>","<i class='icon-arrow-left'></i>"],
    loop: true,
    dots: false,
	autoplay:true,
	autoplayTimeout:4000,
	autoplayHoverPause:true,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 1
      },
      1000: {
        items: 1
      }
    }
  })
})

$(document).ready(function() {
  var owl = $('.products .owl-carousel');
  owl.owlCarousel({
    rtl: true,
    margin: 10,
    nav: true,
    navText : ["<i class='icon-arrow-right'></i>","<i class='icon-arrow-left'></i>"],
    loop: true,
    dots: false,
	autoplay:true,
	autoplayTimeout:4000,
	autoplayHoverPause:true,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 3
      },
      1000: {
        items: 5
      }
    }
  })
})

$(document).ready(function() {
  var owl = $('.partner .owl-carousel');
  owl.owlCarousel({
    rtl: true,
    margin: 0,
    nav: true,
    navText : ["<i class='icon-arrow-right'></i>","<i class='icon-arrow-left'></i>"],
    loop: true,
    dots: false,
	autoplay:true,
	autoplayTimeout:4000,
	autoplayHoverPause:true,
    responsive: {
      0: {
        items: 3
      },
      600: {
        items: 6
      },
      1000: {
        items: 8
      }
    }
  })
})

$(document).ready(function() {
  var owl = $('.projectposts.owl-carousel');
  owl.owlCarousel({
    rtl: true,
    margin: 5,
    nav: true,
    navText : ["<i class='icon-arrow-right'></i>","<i class='icon-arrow-left'></i>"],
    loop: true,
    dots: false,
	autoplay:true,
	autoplayTimeout:4000,
	autoplayHoverPause:true,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 2
      },
      1000: {
        items: 4
      }
    }
  })
})


$(document).ready(function() {
  var bigimage = $("#big");
  var thumbs = $("#thumbs");
  //var totalslides = 10;
  var syncedSecondary = true;

  bigimage
    .owlCarousel({
    items: 1,
    slideSpeed: 2000,
    nav: true,
	rtl: true,
    autoplay: true,
    dots: false,
    loop: true,
    responsiveRefreshRate: 200,
    navText: [
      '<i class="fas fa-arrow-left" aria-hidden="true"></i>',
      '<i class="fas fa-arrow-right" aria-hidden="true"></i>'
    ]
  })
    .on("changed.owl.carousel", syncPosition);

  thumbs
    .on("initialized.owl.carousel", function() {
    thumbs
      .find(".owl-item")
      .eq(0)
      .addClass("current");
  })
    .owlCarousel({
    items: 4,
    dots: false,
	rtl: true,
    nav: false,
    navText: [
      '<i class="fa fa-arrow-left" aria-hidden="true"></i>',
      '<i class="fa fa-arrow-right" aria-hidden="true"></i>'
    ],
    smartSpeed: 200,
    slideSpeed: 500,
    slideBy: 4,
    responsiveRefreshRate: 100
  })
    .on("changed.owl.carousel", syncPosition2);

  function syncPosition(el) {
    //if loop is set to false, then you have to uncomment the next line
    //var current = el.item.index;

    //to disable loop, comment this block
    var count = el.item.count - 1;
    var current = Math.round(el.item.index - el.item.count / 2 - 0.5);

    if (current < 0) {
      current = count;
    }
    if (current > count) {
      current = 0;
    }
    //to this
    thumbs
      .find(".owl-item")
      .removeClass("current")
      .eq(current)
      .addClass("current");
    var onscreen = thumbs.find(".owl-item.active").length - 1;
    var start = thumbs
    .find(".owl-item.active")
    .first()
    .index();
    var end = thumbs
    .find(".owl-item.active")
    .last()
    .index();

    if (current > end) {
      thumbs.data("owl.carousel").to(current, 100, true);
    }
    if (current < start) {
      thumbs.data("owl.carousel").to(current - onscreen, 100, true);
    }
  }

  function syncPosition2(el) {
    if (syncedSecondary) {
      var number = el.item.index;
      bigimage.data("owl.carousel").to(number, 100, true);
    }
  }

  thumbs.on("click", ".owl-item", function(e) {
    e.preventDefault();
    var number = $(this).index();
    bigimage.data("owl.carousel").to(number, 300, true);
  });
});

$(document).ready(function() {
$(".sidebar").stick_in_parent();
});


/*
 Sticky-kit v1.1.2 | WTFPL | Leaf Corcoran 2015 | http://leafo.net
*/
(function(){var b,f;b=this.jQuery||window.jQuery;f=b(window);b.fn.stick_in_parent=function(d){var A,w,J,n,B,K,p,q,k,E,t;null==d&&(d={});t=d.sticky_class;B=d.inner_scrolling;E=d.recalc_every;k=d.parent;q=d.offset_top;p=d.spacer;w=d.bottoming;null==q&&(q=0);null==k&&(k=void 0);null==B&&(B=!0);null==t&&(t="is_stuck");A=b(document);null==w&&(w=!0);J=function(a,d,n,C,F,u,r,G){var v,H,m,D,I,c,g,x,y,z,h,l;if(!a.data("sticky_kit")){a.data("sticky_kit",!0);I=A.height();g=a.parent();null!=k&&(g=g.closest(k));
if(!g.length)throw"failed to find stick parent";v=m=!1;(h=null!=p?p&&a.closest(p):b("<div />"))&&h.css("position",a.css("position"));x=function(){var c,f,e;if(!G&&(I=A.height(),c=parseInt(g.css("border-top-width"),10),f=parseInt(g.css("padding-top"),10),d=parseInt(g.css("padding-bottom"),10),n=g.offset().top+c+f,C=g.height(),m&&(v=m=!1,null==p&&(a.insertAfter(h),h.detach()),a.css({position:"",top:"",width:"",bottom:""}).removeClass(t),e=!0),F=a.offset().top-(parseInt(a.css("margin-top"),10)||0)-q,
u=a.outerHeight(!0),r=a.css("float"),h&&h.css({width:a.outerWidth(!0),height:u,display:a.css("display"),"vertical-align":a.css("vertical-align"),"float":r}),e))return l()};x();if(u!==C)return D=void 0,c=q,z=E,l=function(){var b,l,e,k;if(!G&&(e=!1,null!=z&&(--z,0>=z&&(z=E,x(),e=!0)),e||A.height()===I||x(),e=f.scrollTop(),null!=D&&(l=e-D),D=e,m?(w&&(k=e+u+c>C+n,v&&!k&&(v=!1,a.css({position:"fixed",bottom:"",top:c}).trigger("sticky_kit:unbottom"))),e<F&&(m=!1,c=q,null==p&&("left"!==r&&"right"!==r||a.insertAfter(h),
h.detach()),b={position:"",width:"",top:""},a.css(b).removeClass(t).trigger("sticky_kit:unstick")),B&&(b=f.height(),u+q>b&&!v&&(c-=l,c=Math.max(b-u,c),c=Math.min(q,c),m&&a.css({top:c+"px"})))):e>F&&(m=!0,b={position:"fixed",top:c},b.width="border-box"===a.css("box-sizing")?a.outerWidth()+"px":a.width()+"px",a.css(b).addClass(t),null==p&&(a.after(h),"left"!==r&&"right"!==r||h.append(a)),a.trigger("sticky_kit:stick")),m&&w&&(null==k&&(k=e+u+c>C+n),!v&&k)))return v=!0,"static"===g.css("position")&&g.css({position:"relative"}),
a.css({position:"absolute",bottom:d,top:"auto"}).trigger("sticky_kit:bottom")},y=function(){x();return l()},H=function(){G=!0;f.off("touchmove",l);f.off("scroll",l);f.off("resize",y);b(document.body).off("sticky_kit:recalc",y);a.off("sticky_kit:detach",H);a.removeData("sticky_kit");a.css({position:"",bottom:"",top:"",width:""});g.position("position","");if(m)return null==p&&("left"!==r&&"right"!==r||a.insertAfter(h),h.remove()),a.removeClass(t)},f.on("touchmove",l),f.on("scroll",l),f.on("resize",
y),b(document.body).on("sticky_kit:recalc",y),a.on("sticky_kit:detach",H),setTimeout(l,0)}};n=0;for(K=this.length;n<K;n++)d=this[n],J(b(d));return this}}).call(this);